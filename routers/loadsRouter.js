const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getLoads,
  createLoad,
  getActiveLoads,
  nextStateForLoad,
  getLoadByID,
  updateLoad,
  deleteLoad,
  postLoad,
  shippmentInfo,
} = require('../controllers/loadsController');
const {checkForShipper, checkForDriver} = require('./middlewares/checkForRole');

router.get(
    '/',
    authMiddleware,
    asyncWrapper(getLoads),
);

router.post(
    '/',
    authMiddleware,
    asyncWrapper(checkForShipper),
    asyncWrapper(createLoad),
);

router.get(
    '/active',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(getActiveLoads),
);

router.patch(
    '/active/state',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(nextStateForLoad),
);

router.get(
    '/:id',
    authMiddleware,
    asyncWrapper(getLoadByID),
);

router.put(
    '/:id',
    authMiddleware,
    asyncWrapper(checkForShipper),
    asyncWrapper(updateLoad),
);

router.delete(
    '/:id',
    authMiddleware,
    asyncWrapper(checkForShipper),
    asyncWrapper(deleteLoad),
);

router.post(
    '/:id/post',
    authMiddleware,
    asyncWrapper(checkForShipper),
    asyncWrapper(postLoad),
);

router.get(
    '/:id/shipping_info',
    authMiddleware,
    asyncWrapper(checkForShipper),
    asyncWrapper(shippmentInfo),
);

module.exports = router;

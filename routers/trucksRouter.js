const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getTrucks,
  createTruck,
  getTruckByID,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/trucksController');
const {checkForDriver} = require('./middlewares/checkForRole');
const {checkForAssign} = require('./middlewares/checkForAssign');

router.get(
    '/',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(getTrucks),
);

router.post(
    '/',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(createTruck),
);

router.get(
    '/:id',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(getTruckByID),
);

router.put(
    '/:id',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(checkForAssign),
    asyncWrapper(updateTruck),
);

router.delete(
    '/:id',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(checkForAssign),
    asyncWrapper(deleteTruck),
);

router.post(
    '/:id/assign',
    authMiddleware,
    asyncWrapper(checkForDriver),
    asyncWrapper(checkForAssign),
    asyncWrapper(assignTruck),
);

module.exports = router;

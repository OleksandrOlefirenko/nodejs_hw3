const {Truck} = require('../../models/truckModel');

module.exports.checkForAssign = async (req, res, next) => {
  const {id} = req.params;
  const truck = await Truck.findById(id);

  if (truck['assigned_to'] == req.user._id) {
    req.user.truck = 'assigned';
    next();
  } else {
    next();
  }
};

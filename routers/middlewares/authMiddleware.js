const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');
const {User} = require('../../models/userModel');

module.exports.authMiddleware = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401)
        .json({message: `No Authorization http header found!`});
  }

  const [, token] = header.split(' ');

  if (!token) {
    return res.status(401).json({message: `No JWT token found!`});
  }

  user = jwt.verify(token, JWT_SECRET);
  req.user = await User.findById(user._id);
  next();
};

module.exports.checkForDriver = async (req, res, next) => {
 req.user.role === 'DRIVER' ?
 next() :
 res.status(403).json({message: 'You have not permission to do that'});
};

module.exports.checkForShipper = async (req, res, next) => {
  req.user.role === 'SHIPPER' ?
  next() :
  res.status(403).json({message: 'You have not permission to do that'});
};

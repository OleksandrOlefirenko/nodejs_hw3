const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getUserInfo,
  deleteUser,
  updateUserPassword,
} = require('../controllers/meController');

router.get('/me', authMiddleware, asyncWrapper(getUserInfo));

router.delete('/me', authMiddleware, asyncWrapper(deleteUser));

router.patch('/me/password', authMiddleware, asyncWrapper(updateUserPassword));

module.exports = router;

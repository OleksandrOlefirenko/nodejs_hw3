# HW3 Node js

## Installation
1 Download node.js [node.js](https://nodejs.org/en/) to install node js.

2 Clone repositery
```bash
git clone https://gitlab.com/OleksandrOlefirenko/nodejs_hw3.git
```
3 Use npm install to download all dependencies 
```bash
npm install
```

4 Use npm start to start application
```bash
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
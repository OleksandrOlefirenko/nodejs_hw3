const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {role, email, password} = req.body;

  const userFromDB = await User.findOne({email});

  if (userFromDB) {
    return res.status(400).json({message: `user with such email is present`});
  }

  const user = new User({
    role,
    email,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with username '${email}' found!`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);
  res.json({'message': 'Success', 'jwt_token': token});
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;

  const userFromDB = await User.findOne({email});

  if (!userFromDB) {
    return res.status(400).json({message: `No such person with than email`});
  }

  if (!email) {
    return res.status(400).json({message: `Please write your email`});
  }

  res.status(200).json({message: 'New password sent to your email address'});
};

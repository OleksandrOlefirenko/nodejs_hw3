const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {loadStates} = require('../localDB/loadStates');
const {truckTypes} = require('../localDB/truckTypes');

module.exports.getLoads = async (req, res) => {
  const {offset, limit, status} = req.query;
  const requestOptions = {
    offset: parseInt(offset),
    limit: parseInt(limit),
  };

  const loads = req.user.role === 'SHIPPER' ?
  await Load.find({created_by: req.user._id},
      {__v: 0, created_date: 0}, requestOptions) :
  await Load.find({assigned_to: req.user._id},
      {__v: 0, created_date: 0}, requestOptions);

  const filtredByStatus = status ?
  loads.filter((load) => load.status === status) :
  loads;

  res.status(200).json({loads: filtredByStatus});
};

module.exports.createLoad = async (req, res) => {
  const {
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;

  const load = new Load({
    created_by: req.user._id,
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    logs: [{message: 'Load created', time: Date.now()}],
  });

  await load.save();

  res.status(200).json({message: 'Load created successfully'});
};

module.exports.getActiveLoads = async (req, res) => {
  const load = await Load
      .findOne({assigned_to: req.user._id, status: 'ASSIGNED'}, {__v: 0});


  if (!load) {
    res.status(400).json({message: 'No active loads now'});
  } else {
    res.status(200).json({load});
  }
};

module.exports.nextStateForLoad = async (req, res) => {
  const load = await Load
      .findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
  const currentState = load.state;
  const index = loadStates.indexOf(currentState);
  const nextState = loadStates[index + 1];

  const newLog = {
    message: `Load state changed to ${nextState}`,
    time: Date.now(),
  };

  await Load.updateOne(load, {
    $set: {state: nextState},
    $push: {logs: newLog},
  });

  if (index == 2) {
    await Truck.updateOne({assigned_to: req.user._id}, {$set: {status: 'IS'}});
    await Load.updateOne({_id: load._id}, {$set: {status: 'SHIPPED'}});
  }

  res.status(200).json({message: `Load state changed to ${nextState}`});
};

module.exports.getLoadByID = async (req, res) => {
  const load = await Load
      .findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});

  res.status(200).json({load});
};

module.exports.updateLoad = async (req, res) => {
  const load = await Load
      .findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});

  if (load.status !== 'NEW') {
    res.status(400)
        .json({message: `Just loads with status 'NEW' could be updated`});
  } else {
    await Load.updateOne(load, req.body);
    res.status(200).json({message: 'Load details changed successfully'});
  }
};

module.exports.deleteLoad = async (req, res) => {
  const load = await Load
      .findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});

  if (load.status !== 'NEW') {
    res.status(400)
        .json({message: `Just loads with status 'NEW' could be deleted`});
  } else {
    await Load.remove(load);
    res.status(200).json({message: 'Load deleted successfully'});
  }
};

module.exports.postLoad = async (req, res) => {
  const load = await Load
      .findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});

  if (load.status !== 'NEW') {
    res.status(400)
        .json({message: `Just loads with status 'NEW' could be updated`});
  } else {
    const newLog = {
      message: `Load state changed to POSTED`,
      time: Date.now(),
    };

    await Load.updateOne(load, {
      $set: {status: 'POSTED'},
      $push: {logs: newLog},
    });

    const {payload} = load;
    const {length, width, height} = load.dimensions;
    let freeTruck;
    const availableSizes = [];

    for (let i = 0; i < truckTypes.length; i++) {
      if (truckTypes[i].width > width &&
         truckTypes[i].length > length &&
         truckTypes[i].height >= height &&
         truckTypes[i].payload >= payload) {
        availableSizes.push(truckTypes[i].type);
      }
    }

    for (let i = 0; i < availableSizes.length; i++) {
      freeTruck = await Truck.findOne({
        status: 'IS',
        assigned_to: {$ne: null},
        type: availableSizes[i],
      });

      if (freeTruck) {
        break;
      }
    }

    if (!freeTruck) {
      const newLog = {
        message: `No free truck for you. Status of load changed back for 'NEW'`,
        time: Date.now(),
      };

      await Load.updateOne({_id: req.params.id}, {
        $set: {status: 'NEW'},
        $push: {logs: newLog},
      });

      res.status(400).json({message: 'Not found free Truck for you'});
    } else {
      const newLog = {
        message: `Load was assigned by driver with id ${freeTruck.assigned_to}`,
        time: Date.now(),
      };

      await Load.updateOne({_id: req.params.id}, {
        $set: {
          status: 'ASSIGNED',
          assigned_to: freeTruck.assigned_to,
          state: loadStates[0]},
        $push: {logs: newLog},
      });

      await Truck.updateOne(freeTruck, {$set: {status: 'OL'}});
      res.status(200)
          .json({message: 'Load posted successfully', driver_found: true});
    }
  }
};

module.exports.shippmentInfo = async (req, res) => {
  const load = await Load
      .findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});

  if (!load.assigned_to) {
    res.status(200).json({load});
  }

  const truck = await Truck.findOne({assigned_to: load.assigned_to}, {__v: 0});
  res.status(200).json({load, truck});
};

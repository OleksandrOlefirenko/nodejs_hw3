const {Truck} = require('../models/truckModel');

module.exports.getTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id}, {__v: 0});
  res.status(200).json({trucks});
};

module.exports.createTruck = async (req, res) => {
  const {type} = req.body;

  const truck = new Truck({
    created_by: req.user._id,
    type,
  });

  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
};

module.exports.getTruckByID = async (req, res) => {
  const {id} = req.params;
  const truck = await Truck.findOne({_id: id}, {__v: 0});
  res.status(200).json({truck: truck});
};

module.exports.updateTruck = async (req, res) => {
  const {id} = req.params;
  const {type} = req.body;
  const assigned = req.user.truck;

  if (assigned) {
    res.status(403).json({message: 'You can not update assigned truck'});
  } else {
    const truck = await Truck.findById(id);
    await Truck.updateOne(truck, {
      type,
    });

    res.status(200).json({message: 'Truck details changed successfully'});
  }
};

module.exports.deleteTruck = async (req, res) => {
  const {id} = req.params;
  const assigned = req.user.truck;

  if (assigned) {
    res.status(403).json({message: 'You can not delete assigned truck'});
  } else {
    const truck = await Truck.findById(id);
    await Truck.deleteOne(truck);

    res.status(200).json({message: 'Truck deleted successfully'});
  }
};

module.exports.assignTruck = async (req, res) => {
  const {id} = req.params;
  const assigned = req.user.truck;

  if (assigned) {
    res.status(403).json({message: 'This truck assinged'});
  } else {
    const truck = await Truck.findById(id);
    await Truck.updateOne(truck, {
      assigned_to: req.user._id,
    });

    res.status(200).json({message: 'Truck assigned successfully'});
  }
};

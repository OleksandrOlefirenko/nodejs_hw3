const bcrypt = require('bcrypt');

module.exports.getUserInfo = async (req, res) => {
  const user = req.user;

  if (user) {
    res.status(200).json({
      user: {
        _id: user._id,
        email: user.email,
        created_date: user.created_date,
      },
    });
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
};

module.exports.deleteUser = async (req, res) => {
  const user = req.user;

  if (user.role !== 'SHIPPER') {
    res.status(400)
        .json({message: 'You can`t delete your account'});
  } else {
    await user.remove();
    res.status(200).json({message: 'Success'});
  }
};

module.exports.updateUserPassword = async (req, res) => {
  const user = req.user;

  const {oldPassword, newPassword} = req.body;

  if (oldPassword && newPassword) {
    if (!(await bcrypt.compare(oldPassword, user.password))) {
      return res.status(400).json({message: `Wrong password!`});
    } else {
      if (newPassword != oldPassword) {
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        res.status(200).json({message: 'Success'});
      }
    }
  } else {
    res.status(400).json({message: 'Enter all needed credentials!'});
  }
};

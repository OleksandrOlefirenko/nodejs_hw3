const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const {PORT} = require('./config');

const authRouter = require('./routers/authRouter');
const meRouter = require('./routers/meRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', meRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  if (err) {
    res.status(400).json({message: 'Client Error'});
  }
  res.status(500).json({message: 'Server Error'});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://testuser:testuser@cluster0.zaszh.mongodb.net/hw3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();


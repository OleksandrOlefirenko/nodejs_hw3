module.exports.truckTypes = [
  {
    type: 'SMALL STRAIGHT',
    length: 500,
    width: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'SPRINTER',
    length: 300,
    width: 250,
    height: 170,
    payload: 1700,
  },

  {
    type: 'LARGE STRAIGHT',
    length: 700,
    width: 350,
    height: 200,
    payload: 4000,
  },
];
